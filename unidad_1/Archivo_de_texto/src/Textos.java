import java.io.*;
public class Textos {

	public void leer(String archivo)
	{
		try 
		{
			FileReader r = new FileReader(archivo);
			BufferedReader buffer = new BufferedReader(r);
			String temp="";
			while(temp!=null)
			{
				temp = buffer.readLine();
				if(temp==null)
					break;	
				System.out.println(temp);
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
}
