﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Alta_y_Consulta_MySQL
{
    class Clase
    {
        public MySqlConnection cn;   
        public void conexion()
        {
            String conexionString = "server=localhost;port=3306;user id=root; Password=''; database=bd1 ";
            cn = new MySqlConnection(conexionString);
            try { 
                 cn.Open();
            }catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        public void alta(String cadenaAlta, Label lb, String mensaje)
        {
            try { 
            MySqlCommand cm = new MySqlCommand(cadenaAlta,cn);
            cm.CommandTimeout = 60;
            cm.ExecuteNonQuery();
            cn.Close();
            //MySqlDataReader reader;
            lb.Text = mensaje;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ocurrio un error inesperado.");
            }

        }
        public void consulta(String cadenaConsulta, Label lb)
        {
            MySqlCommand cm = new MySqlCommand(cadenaConsulta, cn);
            cm.CommandTimeout = 60;
            object obj = cm.ExecuteScalar();
            string resultado = Convert.ToString(obj);
            if(String.IsNullOrEmpty(resultado))
            {
                lb.Text = "Registro no encontrado. Intente de nuevo.";
            }
            else
            {
                lb.Text = resultado;

            }
            cn.Close();
        }

    }
}
