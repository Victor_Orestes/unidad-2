package Paquete;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Clase conexion = new Clase();
		String captura = JOptionPane.showInputDialog("Elige entre el 1 o 2.");
		int valor =  Integer.parseInt(captura);
		switch(valor)
		{
			case 1:
			{
				conexion.Mensaje();
				break;
			}
			case 2:
			{
				conexion.Edad();
				break;
			}
			default:
				JOptionPane.showMessageDialog(null, "No elegiste una opci�n valida.");
				break;
		}
		String confirmar = conexion.Confirmar();
		JOptionPane.showMessageDialog(null, "Ejemplo de funci�n donde retorna una cadena de texto. En este caso la edad antes confirmada de "+confirmar+" a�os.");
		ClaseHijo hijo;
		ClaseVecino vecino;
		hijo = new ClaseHijo();
		vecino = new ClaseVecino();
		hijo.A�o(confirmar);
		vecino.A�o(confirmar);
	}

}
