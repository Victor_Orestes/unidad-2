﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Alta_y_Consulta_MySQL
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

    }

        private void Form1_Load(object sender, EventArgs e)
        {
            descripciónTxt.Focus();
        }

        private void AltaBTN_Click(object sender, EventArgs e)
        {
            Clase cl = new Clase();
            cl.conexion();
            String cadena = "insert into articulos values ('','" + descripciónTxt.Text + "',"+Int32.Parse(precioTxt.Text)+")";
            cl.alta(cadena,resultadoConsulta,"Alta exitosa.");
            descripciónTxt.Clear();
            precioTxt.Clear();
            descripciónTxt.Focus();
            
        }

        private void ConsultaBTN_Click(object sender, EventArgs e)
        {
            Clase cl = new Clase();
            cl.conexion();
            String cadena = "select descripcion,precio from articulos where codigo=" + Int32.Parse(consultaTxt.Text);
            cl.consulta(cadena,resultadoConsulta);
            consultaTxt.Clear();
            consultaTxt.Focus();
        }

        private void FormBTN_Click(object sender, EventArgs e)
        {
            Form2 frm = new Form2();
            frm.ShowDialog();
        }
    }
}
