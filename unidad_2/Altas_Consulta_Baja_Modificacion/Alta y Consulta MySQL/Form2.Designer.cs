﻿namespace Alta_y_Consulta_MySQL
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateBTN = new System.Windows.Forms.Button();
            this.precioTxt = new System.Windows.Forms.TextBox();
            this.descripciónTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.consultaBTN = new System.Windows.Forms.Button();
            this.consultaTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.resultadoConsulta = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.codigoTxt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // updateBTN
            // 
            this.updateBTN.Location = new System.Drawing.Point(272, 202);
            this.updateBTN.Name = "updateBTN";
            this.updateBTN.Size = new System.Drawing.Size(147, 50);
            this.updateBTN.TabIndex = 8;
            this.updateBTN.Text = "Modificar";
            this.updateBTN.UseVisualStyleBackColor = true;
            this.updateBTN.Click += new System.EventHandler(this.UpdateBTN_Click);
            // 
            // precioTxt
            // 
            this.precioTxt.Location = new System.Drawing.Point(328, 151);
            this.precioTxt.Name = "precioTxt";
            this.precioTxt.Size = new System.Drawing.Size(150, 31);
            this.precioTxt.TabIndex = 7;
            // 
            // descripciónTxt
            // 
            this.descripciónTxt.Location = new System.Drawing.Point(328, 89);
            this.descripciónTxt.Name = "descripciónTxt";
            this.descripciónTxt.Size = new System.Drawing.Size(250, 31);
            this.descripciónTxt.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(71, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "Precio:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(71, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Descripción del artículo:";
            // 
            // consultaBTN
            // 
            this.consultaBTN.Location = new System.Drawing.Point(207, 319);
            this.consultaBTN.Name = "consultaBTN";
            this.consultaBTN.Size = new System.Drawing.Size(271, 50);
            this.consultaBTN.TabIndex = 12;
            this.consultaBTN.Text = "Eliminar por código";
            this.consultaBTN.UseVisualStyleBackColor = true;
            this.consultaBTN.Click += new System.EventHandler(this.ConsultaBTN_Click);
            // 
            // consultaTxt
            // 
            this.consultaTxt.Location = new System.Drawing.Point(460, 268);
            this.consultaTxt.Name = "consultaTxt";
            this.consultaTxt.Size = new System.Drawing.Size(200, 31);
            this.consultaTxt.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 274);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(405, 25);
            this.label4.TabIndex = 10;
            this.label4.Text = "Ingrese el código del artículo a consultar:";
            // 
            // resultadoConsulta
            // 
            this.resultadoConsulta.AutoSize = true;
            this.resultadoConsulta.Location = new System.Drawing.Point(44, 395);
            this.resultadoConsulta.Name = "resultadoConsulta";
            this.resultadoConsulta.Size = new System.Drawing.Size(109, 25);
            this.resultadoConsulta.TabIndex = 9;
            this.resultadoConsulta.Text = "Resultado";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Código";
            // 
            // codigoTxt
            // 
            this.codigoTxt.Location = new System.Drawing.Point(328, 25);
            this.codigoTxt.Name = "codigoTxt";
            this.codigoTxt.Size = new System.Drawing.Size(100, 31);
            this.codigoTxt.TabIndex = 5;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 487);
            this.Controls.Add(this.consultaBTN);
            this.Controls.Add(this.consultaTxt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.resultadoConsulta);
            this.Controls.Add(this.updateBTN);
            this.Controls.Add(this.precioTxt);
            this.Controls.Add(this.codigoTxt);
            this.Controls.Add(this.descripciónTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form2";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Baja y modificación MySQL";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button updateBTN;
        private System.Windows.Forms.TextBox precioTxt;
        private System.Windows.Forms.TextBox descripciónTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button consultaBTN;
        private System.Windows.Forms.TextBox consultaTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label resultadoConsulta;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox codigoTxt;
    }
}