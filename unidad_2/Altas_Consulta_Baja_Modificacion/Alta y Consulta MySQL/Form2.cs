﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Alta_y_Consulta_MySQL
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void UpdateBTN_Click(object sender, EventArgs e)
        {
            try
            { 
                Clase cl = new Clase();
                cl.conexion();
                String cadena = "update articulos set descripcion='" + descripciónTxt.Text + "', precio=" + Int32.Parse(precioTxt.Text) + " where codigo=" + Int32.Parse(codigoTxt.Text);
                cl.alta(cadena,resultadoConsulta,"Registro modificado.");
                descripciónTxt.Clear();
                codigoTxt.Clear();
                precioTxt.Clear();
                codigoTxt.Focus();
            }
            catch(Exception)
            {
                MessageBox.Show("Ocurrio un error durante la actualización de datos.");
            }
        }

        private void ConsultaBTN_Click(object sender, EventArgs e)
        {
            try
            { 
                Clase cl = new Clase();
                cl.conexion();
                String cadena = "delete from articulos where codigo=" + Int32.Parse(consultaTxt.Text);
                cl.alta(cadena,resultadoConsulta,"Registro eliminado.");
                consultaTxt.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error al eliminar el registro.");
            }
        }
    }
}
