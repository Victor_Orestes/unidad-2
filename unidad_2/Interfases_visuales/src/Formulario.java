import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
public class Formulario extends JFrame implements ActionListener{
	private JTextField textfield,num1,num2;
	private JTextArea area;
	private JScrollPane panel;	
	private JComboBox combo1,combo2,combo3;
	private JLabel label1, label2, label3, sum1,sum2,colVerde,colRojo,colAzul;//declaraci�n de variable tipo Etiqueta
	private JButton boton1,boton2,btnSum,btnComprobar,btnColor; // declaraci�n de variable tipo Bot�n.
    private JMenuBar mb;
    private JMenu menu1,menu2,menu3;
    private JMenuItem mi1,mi2,mi3,mi4;
    private JLabel label4;
    private JCheckBox check1;
    private JButton boton3;
    
	public Formulario() 
	{
		setLayout(null);
		label1 = new JLabel("Sistema de Facturaci�n.");//Agregar texto a etiquetas
		label2 = new JLabel("Versi�n 1.0");
		label3 = new JLabel("T�tulo para el sistema:");
		sum1 = new JLabel("N�mero 1:");
		sum2 = new JLabel("N�mero 2:");
		combo1 = new JComboBox();
		combo2 = new JComboBox();
		combo3 = new JComboBox();
		for(int x=0;x<=255;x++)
		{
			combo1.addItem(String.valueOf(x));
		}
		for(int y=0;y<=255;y++)
		{
			combo2.addItem(String.valueOf(y));
		}
		for(int z=0;z<=255;z++)
		{
			combo3.addItem(String.valueOf(z));
		}
		textfield = new JTextField();
		label1.setBounds(10,0,300,30);
		label2.setBounds(10,550,100,30);
		label3.setBounds(10,20,200,30);
		sum1.setBounds(10, 45, 100,30);
		sum2.setBounds(10, 65, 100,30);
		colVerde = new JLabel("Verde");
		colVerde.setBounds(550,30,150,30);
		combo1.setBounds(650,30,150,30);
		colAzul = new JLabel("Azul");
		colAzul.setBounds(550,70,150,30);
		combo2.setBounds(650,70,150,30);
		colRojo = new JLabel("Rojo");
		colRojo.setBounds(550,110,150,30);
		combo3.setBounds(650,110,150,30);
		textfield = new JTextField();
		area = new JTextArea();
		panel = new JScrollPane(area);
		num1 = new JTextField();
		num2 = new JTextField();
		textfield.setBounds(200,30,150,20);
		num1.setBounds(70, 50, 50, 20);
		num2.setBounds(70, 70, 50, 20);
		area.setBounds(10,150,800,400);
		panel.setBounds(10,130,800,400);
		add(label1); //Agregar al frame
		add(label2);
		add(colVerde);
		add(colAzul);
		add(colRojo);
		add(label3);
		add(sum1);
		add(sum2);
		add(textfield);
		add(num1);
		add(num2);
		add(area);
		add(combo1);
		add(combo2);
		add(combo3);
		//add(panel);
        boton1=new JButton("Cerrar"); //Crear bot�n y leyenda asignada.
        boton2=new JButton("Aceptar");
        btnSum = new JButton("Sumar");
        btnComprobar = new JButton("Verificar");
        boton1.setBounds(700,550,100,30);
        boton2.setBounds(350,30,100,30);
        btnSum.setBounds(40,90,100,30);
        btnComprobar.setBounds(825,150,100,30);
        btnColor = new JButton("Fijar");
        btnColor.setBounds(800,70,100,30);
        add(btnColor);
        add(boton1);
        add(boton2);
        add(btnSum);
        add(btnComprobar);
        boton1.addActionListener(this); //m�todo de acci�n esperada.
        boton2.addActionListener(this);
        btnSum.addActionListener(this);
        //combo1.addActionListener(this);
        btnComprobar.addActionListener(this);
        btnColor.addActionListener(this);
        mb=new JMenuBar();
        setJMenuBar(mb);
        menu1=new JMenu("Opciones");
        mb.add(menu1);
        menu2=new JMenu("Tama�o de la ventana");
        menu1.add(menu2);
        menu3=new JMenu("Color de fondo");
        menu1.add(menu3);
        mi1=new JMenuItem("640*480");
        menu2.add(mi1);
        mi1.addActionListener(this);
        mi2=new JMenuItem("1024*768");
        menu2.add(mi2);
        mi2.addActionListener(this);
        mi3=new JMenuItem("Rojo");
        menu3.add(mi3);
        mi3.addActionListener(this);
        mi4=new JMenuItem("Verde");
        menu3.add(mi4);
        mi4.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e)
	{
        if (e.getSource()==boton1) {
            System.exit(0); //Salida del sistema.
        }
        if (e.getSource()==boton2)
        {
        	String cad=textfield.getText();
        	setTitle(cad);
        }
        if (e.getSource()==btnSum)
        {
        	String n1 = num1.getText();
        	String n2 = num2.getText();
        	try 
        	{
	        	int x1 = Integer.parseInt(n1);
	        	int x2 = Integer.parseInt(n2);
	        	int suma = x1 + x2;
	        	String total = String.valueOf(suma);
	        	JOptionPane.showMessageDialog(null, "El total de la suma es: "+total);
        	}
        	catch(Exception ex)
        	{
        		JOptionPane.showMessageDialog(null, "Imposible realizar la suma.");
        	}
        }
        if (e.getSource()==btnComprobar)
        {
        	String txtArea = area.getText();
        	if (txtArea.indexOf("M�xico")!=-1)
        	{
        		setTitle("Si contiene el texto \"M�xico\"");
        	}
        	else
        	{
        		setTitle("No contiene el texto \"M�xico\"");
        	}
        }
        /*if (e.getSource()==combo1)
        {
        	String seleccion = (String)combo1.getSelectedItem();
        	setTitle("El color seleccionado fue "+seleccion);
        }*/
        if (e.getSource()==btnColor)
        {
        	String valorVerde = (String)combo1.getSelectedItem();
        	String valorAzul = (String)combo2.getSelectedItem();
        	String valorRojo = (String)combo3.getSelectedItem();
        	int verde = Integer.parseInt(valorVerde);
        	int azul = Integer.parseInt(valorAzul);
        	int rojo = Integer.parseInt(valorRojo);
        	Color color1 = new Color(rojo,verde,azul);
        	btnColor.setBackground(color1);
        }
        if (e.getSource()==mi1) {
            setSize(640,480);
        }
        if (e.getSource()==mi2) {
            setSize(1024,768);
        }
        if (e.getSource()==mi3) {
            getContentPane().setBackground(new Color(255,0,0));
        }
        if (e.getSource()==mi4) {
            getContentPane().setBackground(new Color(0,255,0));
        }
	}
}
