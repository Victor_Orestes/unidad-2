package paquete;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ventana extends JFrame implements ActionListener{
    JPanel panel;
    JButton boton;
    JLabel texto;
    JTextField input;
	public Ventana()
	{
		panel = new JPanel();
        boton = new JButton("Oprimir");
        texto = new JLabel();
        input = new JTextField("Escribir aqui");
        
        this.add(panel);
        
        panel.add(boton);
        panel.add(input);
        panel.add(texto);
        
        this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
        
        this.boton.addActionListener(this);
	}
    public void actionPerformed(ActionEvent e){
        this.texto.setText(this.input.getText());
    }
	
}
