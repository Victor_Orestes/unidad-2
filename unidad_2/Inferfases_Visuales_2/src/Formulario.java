import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
public class Formulario extends JFrame implements ActionListener, ChangeListener {
	
	private JLabel label1,label2;
    private JCheckBox check1;
    private JButton boton1;
    private JRadioButton radio1,radio2,radio3;
    private ButtonGroup bg;
	public Formulario()
	{
		setLayout(null);
        label1=new JLabel("�Esta de acuerdo con las normas del servicio?");
        label1.setBounds(10,10,400,30);
        add(label1);
        check1=new JCheckBox("Acepto");
        check1.setBounds(10,50,100,30);
        check1.addChangeListener(this);
        add(check1);
        boton1=new JButton("Continuar");
        boton1.setBounds(10,100,100,30);
        add(boton1);
        boton1.addActionListener(this);
        boton1.setEnabled(false);
        bg=new ButtonGroup();
        radio1=new JRadioButton("640*480");
        radio1.setBounds(10,200,100,30);
        radio1.addChangeListener(this);
        label2 = new JLabel("Elige tama�o de ventana:");
        label2.setBounds(15,180,150,30);
        add(label2);
        add(radio1);
        bg.add(radio1);
        radio2=new JRadioButton("800*600");
        radio2.setBounds(10,230,100,30);
        radio2.addChangeListener(this);        
        add(radio2);
        bg.add(radio2);
        radio3=new JRadioButton("1024*768");
        radio3.setBounds(10,260,100,30);
        radio3.addChangeListener(this);        
        add(radio3);
        bg.add(radio3);      
	}
	public void stateChanged(ChangeEvent e) {
        if (check1.isSelected()==true) {
            boton1.setEnabled(true);
        } else {
            boton1.setEnabled(false);	
        }
        if (radio1.isSelected()) {
            setSize(640,480);
        }
        if (radio2.isSelected()) {
            setSize(800,600);
        }
        if (radio3.isSelected()) {
            setSize(1024,768);
        } 
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==boton1) {
            JOptionPane.showMessageDialog(null, "Evento del boton1");
        }
    }
}
